<?php
$json_link = "http://www.pinkvilla.com/photo-gallery-feed-page";
$json = file_get_contents($json_link);
$obj = json_decode($json, true);
$feed_item_count = count($obj['nodes']);
//echo($feed_item_count);
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
</head>
<body>

<?php


for($x=0; $x<$feed_item_count; $x++){
 
    // to get the post id
    //$title = $obj['nodes'][$x]['node']['title']; ?>
	<article class="article">
		<a href="https://www.pinkvilla.com<?=$obj['nodes'][$x]['node']['path'];?>">
			<div class="article_inner">
				<div class="article_img">
					<img src="https://www.pinkvilla.com<?=$obj['nodes'][$x]['node']['field_photo_image_section'];?>">
				</div>
				<div class="article_content">
					<h5><?=$obj['nodes'][$x]['node']['title'];?></h5>			
				</div>		
			</div>
		</a>
	</article>	
<?php } ?>
<div class="container">

</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script type="text/javascript">

var i = 1;
$(window).scroll(function() {
	if ($(window).scrollTop() >= ($(document).height() - $(window).height() - 10)) {
		console.log("bottom"); 
		getAllData(i);
	}
});


function getAllData(page) {

        var xmlhttp = new XMLHttpRequest();
        var url = "http://localhost/php/" + page + ".json";

        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                var response = JSON.parse(this.responseText);
                var data = "";
				for(i = 0; i < response.nodes.length; i++) {
		        data += '<article class="article"><a href="https://www.pinkvilla.com' + response.nodes[i].node.path + '"><div class="article_inner"><div class="article_img"><img src="https://www.pinkvilla.com' + response.nodes[i].node.field_photo_image_section + '"></div><div class="article_content"><h5>' + response.nodes[i].node.title + '</h5></div></div></a></article>'

				}
				document.getElementsByClassName("container")[0].innerHTML = data;                
            }
        };
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
}


</script>
</body>
</html>